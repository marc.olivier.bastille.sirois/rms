<?php

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', [HomeController::class, 'index']);


Route::get('/', function () {
    return view('welcome');
});


// Products
Route::get('/products', function () {
    return view('products.index');
});

Route::post('/products', [ProductsController::class, 'create']);
Route::put('/products/{product}-{slug}', [ProductsController::class, 'update']);
Route::delete('/products/{product}-{slug}', [ProductsController::class, 'delete']);


// Categories 
Route::post('/categories', [CategoriesController::class, 'create']);
