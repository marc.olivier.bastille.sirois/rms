<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Nullable;

class ProductsController extends Controller
{
    public function create() 
    {
        $product = Product::create($this->validateRequest());

        return redirect($product->path());
    }

    public function update(Product $product) 
    {
        $product->update($this->validateRequest());

        return redirect($product->path());
    }

    public function delete(Product $product) 
    {
        $product->delete($product);

        return redirect('/products');
    }

    protected function validateRequest() {
        return request()->validate([
            'name' => 'string|required',
            'category' => 'string|nullable',
            'rating' => 'integer|between:0,10|nullable',
            'user_id' => 'integer|required'
        ]);
    }
}
