<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function create() 
    {
        Category::create($this->validateRequest());
    }

    protected function validateRequest() {
        return request()->validate([
            'name' => 'string|required'
        ]);
    }
}
