<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function basePath(): string
    {
        return '/categories';
    }

    public function path(): string
    {
        return '/category/' . $this->id;
    }
}
