<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function basePath(): string
    {
        return '/products/';
    }

    public function path(): string
    {
        return $this->basePath() . $this->id . '-' . Str::slug($this->name);
    }
}
