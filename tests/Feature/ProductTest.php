<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;

    private function validProduct (): Array {
        return [
            'name' => 'Product Name',
            'category' => 'Product Category',
            'rating' => 10,
            'user_id' => 1
        ];
    }

    private function createValidProduct (): TestResponse {
        return $this->post(Product::basePath(), $this->validProduct());
    }


    /** @test */
    public function a_new_product_can_be_created()
    {       
        $response = $this->createValidProduct();
        $product = Product::first();

        $this->assertCount(1, Product::all());
        $response->assertRedirect($product->path());
    }

    /** @test */
    public function product_name_is_mandatory()
    {             
        $response = $this->post(Product::basePath(), array_merge($this->validProduct(), ['name' => null]));

        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function product_category_is_optional()
    {        
        $response = $this->post(Product::basePath(), array_merge($this->validProduct(), ['category' => null]));

        $response->assertSessionHasNoErrors('category');
    }

    /** @test */
    public function product_rating_is_optional()
    {        
        $response = $this->post(Product::basePath(), array_merge($this->validProduct(), ['rating' => null]));

        $response->assertSessionHasNoErrors('rating');
    }

    /** @test 
    *   @depends a_new_product_can_be_created
    */
    public function a_product_can_be_updated()
    {     
        $response = $this->createValidProduct();

        $product = Product::first();
        $newProductName = 'New Product Name';
        $newCategory = 'New Category';
        $newRating = 8;

        $response = $this->put($product->path(), [
            'name' => $newProductName,
            'category' => $newCategory,
            'rating' => $newRating,
            'user_id' => $product->user_id
        ]);

        $product = $product->fresh();

        $this->assertEquals($newProductName, $product->name);
        $this->assertEquals($newCategory, $product->category);
        $this->assertEquals($newRating, $product->rating);
        $response->assertRedirect($product->path());
    }

    /** @test 
    *   @depends a_new_product_can_be_created
    */    
    public function a_product_can_be_deleted()
    {       
        $this->createValidProduct();     
        
        $response = $this->delete(Product::first()->path());

        $this->assertCount(0, Product::all());

        $response->assertRedirect(Product::basePath());
    }
}
