<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Category;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CategoryTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;


    private function validCategory (): Array {
        return [
            'name' => 'Category Name'
        ];
    }


    /** @test */
    public function a_new_category_can_be_created()
    {
        $this->post(Category::basePath(), $this->validCategory());

        $this->assertCount(1, Category::all());
    }

        /** @test */
        public function category_name_is_required()
        {
            $response = $this->post(Category::basePath(), array_merge($this->validCategory(), ['name' => null]));

            $response->assertSessionHasErrors('name');
        }
}
