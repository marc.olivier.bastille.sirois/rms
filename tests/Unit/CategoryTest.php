<?php

namespace Tests\Unit;

use App\Models\Category;
use Tests\TestCase;
use PHPUnit\Framework\TestCase as UnitTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CategoryTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;
   
    /** @test */
    public function only_name_is_mandatory_to_create_a_category()
    {   
        Category::firstOrCreate([
            'name' => 'Category Name'
        ]);

        $this->assertCount(1, Category::all());
    }
}
