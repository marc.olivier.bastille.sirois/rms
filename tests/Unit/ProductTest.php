<?php

namespace Tests\Unit;

use App\Models\Product;
use Tests\TestCase;
use PHPUnit\Framework\TestCase as UnitTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    use RefreshDatabase;
    use DatabaseMigrations;
   
    /** @test */
    public function name_and_user_id_are_mandatory_to_create_a_product()
    {   
        Product::firstOrCreate([
            'name' => 'Product Name',
            'user_id' => 1
        ]);

        $this->assertCount(1, Product::all());
    }
}
